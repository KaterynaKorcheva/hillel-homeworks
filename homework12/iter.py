

class CustomIterator:
    ''''
    class describes sequencies access
    '''
    def __init__(self, sequence, start, end):
        self.sequence = sequence
        if start < 0 or start >= (len(sequence)):
            raise ValueError
        if end < 0 or end >= (len(sequence)):
            raise ValueError
        if start > end:
            raise ValueError
        self.end = end
        self.position = start

    def __iter__(self):
        ''''
        define iteration function
        '''
        return self

    def __next__(self):
        ''''
        define conditions of iteration
        '''
        if self.position <= self.end:
            item = self.sequence[self.position]
            self.position+=1
            return item
        else:
            raise StopIteration

