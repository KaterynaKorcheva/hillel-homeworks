from Human import Human
from iter import CustomIterator

if __name__ == "__main__":
    # create objects of a class Human and sequence of an appearance
    humans = []
    humans.append(Human("Frank", 26, "skayting"))
    humans.append(Human("Ann", 20, "skiing"))
    humans.append(Human("Nikita", 36, "swimming"))
    humans.append(Human("Mari", 11, "barbi"))
    for item in humans:
        print(f"I\'m {item.name} and I like {item.action}")

    print("-------------------------------")

    custom_iter = CustomIterator(humans, 1, 2)
    while True:
        try:
            obj = next(iter(custom_iter))
            print(f"I\'m {obj.name} and I like {obj.action}")
        except StopIteration:
            break