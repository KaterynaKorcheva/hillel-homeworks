from Action import Action

class Human:
    ''''
    describe Human class, basic info
    '''
    def __init__(self, name, age, action_name):
        ''''

        \brief simple class initializer

        \param name           The name of the Human
        \param age            Age of the Human
        \param action_name    Name of an action

        '''
        self.__name = name
        self.__age = age
        self.__action = Action(action_name)

    @property
    def name(self):
        ''''
        function definition for Name of a Human
        '''
        return self.__name

    @property
    def action(self):
        ''''
        function definition for action of a Human
        '''
        return self.__action()
