class Action:
    ''''
     describe Action class, basic info
    '''
    def __init__(self, action_name: str):
        ''''
         \brief simple class initializer
        '''
        self.action_name = action_name

    def __call__(self):
        ''''
        execute function by it's string name
        '''
        return self.action_name



