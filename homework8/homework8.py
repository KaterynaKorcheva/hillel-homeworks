#1. Напишіть функцію, яка на вхід буде приймати список стрінгів, а в результаті повертати список в якому всі стрінги будуть у верхньому регістрі
#Використати для цього функцію map()
#Наприклад на вхід: ['alfred', 'ben', 'william']
#а на вихід: ['ALFRED', 'BEN', 'WILLIAM']

# def do_capitalize(lst):
#     return list(map(lambda x: x.upper(), lst))
#
# names = ['alfred', 'ben', 'william']
# print(do_capitalize(names))


#def upper_names():
#    str_names = ['alfred', 'ben', 'william']
#    res = list(map(str.upper, str_names))
#    print(res)

#func()

#2. Напишіть функцію, яка на вхід буде приймати список флоатів, а в результаті повертати список в якому всі числа будуть піднесені в степінь 2 і заокруглені до трьох знаків після коми
#Використати для цього функцію map()
#Наприклад на вхід: [4.35, 6.09, 3.25, 9.77, 2.16, 8.88, 4.59]
#а на вихід: [18.922, 37.088, 10.562, 95.453, 4.666, 78.854, 21.068]

numbers = [4.35, 6.09, 3.25, 9.77, 2.16, 8.88, 4.59]

def square_and_round(lst, prec):
    return list(map(lambda x: round(x * x, prec), lst))

print(square_and_round(numbers,3))


#3. Напишіть функцію, яка на вхід буде приймати два списки, а в результаті повертати список в якому попарно об'єднані в тупли елементи
#Використати для цього функцію zip()
#Наприклад на вхід: ['a', 'b', 'c', 'd', 'e'], [1,2,3,4,5]
#а на вихід: [('a', 1), ('b', 2), ('c', 3), ('d', 4), ('e', 5)]

letters =['a', 'b', 'c', 'd', 'e']
numbers = [1,2,3,4,5]

def do_dict(keys, items):
    return dict(zip(keys, items))

print(do_dict(letters, numbers))

#4. Напишіть функцію, яка на вхід буде приймати список стрінгів і число, а в результаті повертати список в якому
# будуть ті стрінги, довжина яких буде менша за число
#Використати для цього функцію filter()
##Наприклад на вхід: ["Jeff", "Alex", "Jonathan", "Richelle", "Anna"], 5
#а на вихід: ["Jeff", "Alex", "Anna"]

#def do_filter(lst, filter_number):
#    return list(filter(lambda x: len(x) < filter_number, lst))


##list_names_number = ["Jeff", "Alex", "Jonathan", "Richelle", "Anna"]
#print(do_filter(list_names_number, 5))




