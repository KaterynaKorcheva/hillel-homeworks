# create class
class Company:
    '''
    \brief  Describe the IT company basic info
    '''
    # method of initialization
    def __init__(self, name_company, number_staff_Ukr, number_staff_word, number_projects):
        '''
        \brief simple class initializer

        \param name_company         The name of the company
        \param number_staff_Ukr     Number of employees in Ukrainian branch
        \param number_staff_world   Number of employees in other counties
        \param number_projects      Number of actual projects

        '''
        self.name_company = name_company
        self.number_staff_Ukr = number_staff_Ukr
        self.number_staff_word = number_staff_word
        self.number_projects = number_projects

    # function definition for get company name
    def get_name_company(self):
        '''
        \brief getter for Company name field
        '''
        return self.name_company

    # function definition for get number of employees in Ukrainian office
    def get_number_staff_Ukr(self):
        ''''
        \brief getter for number of staff in Ukraine field
        '''
        return self.number_staff_Ukr

    # function definition for get number of employees in a world
    def get_number_staff_word(self):
        ''''
        \brief getter for number of staff in the world field
        '''
        return self.number_staff_word

    # function definition for get number of projects
    def get_number_projects(self):
        ''''
        \brief getter for number of projects field
        '''
        return self.number_projects


#create object of a class Company and asign values to all parameters
Emap = Company("Epam", 13500, 60000, 300)
# accessing value of a name_company parameter
print(Emap.get_name_company())
# accessing value of a number_staff_Ukr parameter
print(Emap.get_number_staff_Ukr())
# accessing value of a number_staff_world parameter
print(Emap.get_number_staff_word())
# accessing value of a number_project parameter
print(Emap.get_number_projects())