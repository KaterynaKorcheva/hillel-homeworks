from employee import Employee
#create object of a class Employee and asign values to all parameters
Richard = Employee("Richard", '23.04.1987', 'TY687456', 'Java Developer', 2500)
# accessing value of a name
print(Richard.get_name())
# accessing value of a date of the birth
print(Richard.get_birth_date())
# accessing value of a passport number
print(Richard.get_passport())
# accessing value of a position
print(Richard.get_position())
# accessing value of a salary
print(Richard.get_salary())
# change the position for the Employee
Richard.set_position("Middle Java Developer")
print(Richard.get_position())
# change the amount of salary
Richard.set_salary(3000)
print(Richard.get_salary())