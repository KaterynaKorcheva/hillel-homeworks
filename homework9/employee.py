# create class for Employee
class Employee:
    '''
    \brief  Describe the Employee of the IT company, basic info
    '''
    # method of initialization
    def __init__(self, name, birth_date, passport, position, salary):
        '''
        \brief simple class initializer

        \param name           The name of the Employee
        \param birth_date     Date of the birth
        \param passport       Passport number
        \param salary         Salary

        '''
        self.name = name
        self.birth_date = birth_date
        self.passport = passport
        self.position = position
        self.salary = salary


    # function definition for get name
    def get_name(self):
        '''
        \brief getter for Employee name field
        '''
        return self.name

    # function definition for get date of bierth
    def get_birth_date(self):
        ''''
        \brief getter for Employee's date of the birth  field
        '''
        return self.birth_date

    # function definition for get passport number
    def get_passport(self):
        ''''
        \brief getter for passport's number   field
        '''
        return self.passport

    # function definition for get job position
    def get_position(self):
        ''''
        \brief getter for Employee's position  field
        '''
        return self.position

    # function definition for get salary of employee
    def get_salary(self):
        ''''
        \brief getter for Employee's salary  field
        '''
        return self.salary

    # function definition for set changes for job position
    def set_position(self, new_position):
        self.position = new_position

    # function definition for set changes for salary of employee
    def set_salary(self, new_salary):
        self.salary = new_salary



