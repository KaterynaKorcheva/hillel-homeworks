from abc import ABC, abstractmethod


class IConditioner(ABC):

    @abstractmethod
    def heart(self):
        """Declare functionality to heart for child classes"""
        pass

    @abstractmethod
    def cool (self):
        """Declare functionality to cool for child classes"""
        pass

    @abstractmethod
    def alarm_condencate (self):
        ''''
        Declare functionality to collect condensate
        '''
        pass

    @abstractmethod
    def swing (self):
        ''''
        Declare functionality to adjust the direction of the cooled air flows in the room.
        '''
        pass