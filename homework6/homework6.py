age_table = {
     '0': "років"
    ,'1': "рік"
    ,'2': "роки"
    ,'3': "роки"
    ,'4': "роки"
    ,'5': "років"
    ,'6': "років"
    ,'7': "років"
    ,'8': "років"
    ,'9': "років"
}

def get_age_string(age):
    '''
    визначення правильної форми слова рік
    '''
    age_string = str(age)
    for key in age_table:
        if key == age_string[-1]:
            return age_string + " " + age_table[key]
    return age_string

def get_yesno(hint):
    '''
    отримання відповіді Yes/No для продовження або закінчення програми
    '''
    while True:
        answer = input(str(hint+" "))
        ret = answer.lower()
        if answer.lower() == "no" or answer.lower() == "yes":
            break
        else:
            print("Wrong input, please type 'yes' or 'no'")
    return ret

def check_interest_age(age):
    '''
    визначення віку, який складається з двох однакових цифр
    '''
    return ( int(age) % 11 ) == 0

def process_age(age):
    '''
    программа Касир у кінотеатрі
    видає різні повідомлення в залежності від введенного віку користувача
    '''
    if not age.isdigit() or int(age) <= 0 or int(age) > 130:
        print("Невірні данні")
        return -1

    age = int(age)

    if check_interest_age(age) == True:
        print(f"О, вам {get_age_string(age)}! Який цікавий вік!")

    if age < 7:
        print(f"Тобі ж {get_age_string(age)}! Де твої батки?")
    elif age <16:
        print(f"Тобі лише {get_age_string(age)}, a це фільм для дорослих!")
    elif age > 65:
        print(f"Вам {get_age_string(age)}? Покажіть пенсійне посвідчення!")
    else:
        print(f"Незважаючи на те, що вам {get_age_string(age)} білетів всеодно нема!")

    return 0

def main():
    '''
    основна функція, користувач вводить свій вік, після отримання результату запитує, чи повторити виконання програми
    або завершити її
    '''
    while True:
        age = input("Введіть, будь ласка, свій вік  ")
        process_age(age)
        if get_yesno("Повторити виконання?") == "no":
            break

    return 0

main()


