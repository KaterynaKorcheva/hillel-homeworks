 # Є лист довільних чисел [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44].
 # Напишіть код, який видалить (не створить новий, а саме видалить!) з нього всі числа, які менше 21 і більше 74.

lst = [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44]
lst1 = lst.copy()

for elem in lst1:
    if elem < 21 or elem > 74:
        lst.remove(elem)
print(lst)

# Є два довільних числа які відповідають за мінімальну і максимальну ціну.
 # Є Dict з назвами магазинів і цінами:
 # { "cito": 47.999,
 # "BB_studio" 42.999,
 # "momo": 49.999,
 # "main-service": 37.245,
 # "buy.now": 38.324,
 # "x-store": 37.166,
 # "the_partner": 38.988,
 # "store": 37.720,
 # "rozetka": 38.003}.
 # Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких попадають в діапазон між мінімальною
 # і максимальною ціною.
 # Наприклад:
 #
 # lower_limit = 35.9
 #
 # upper_limit = 37.339
 #
 # > match: "x-store", "main-service"

store_dict = {
     "cito": 47.999
    ,"BB_studio": 42.999
    ,"momo": 49.999
    ,"main-service": 37.245
    ,"buy.now": 38.324
    ,"x-store": 37.166
    ,"the_partner": 38.988
    ,"store": 37.720
    ,"rozetka": 38.003
}
lower_limit = 35.9
upper_limit = 37.339
for key in store_dict:
    if store_dict[key] >= lower_limit and store_dict[key] <= upper_limit:
        print(key)


    # if lower_limit > 35.9 or upper_limit < 37.339
    # print("key:", item)