from SmartScalesBase import SmartScales

class Tefal(SmartScales):
    ''''
    daughter's class to describe SmartScale Tefal
    '''
    def __init__(self, max_weight, bluetooth, measure_units, number_users, price):
        super().__init__(max_weight, bluetooth, measure_units, number_users, price)


    def receive_data(self):
        ''''
        describe function receive_data for Tefal Smartscales by tablet
        '''
        print("Receive data by tablet")

    def transfer_data(self):
        ''''
        describe the path to send data by bluetooth version
        '''
        print("Connection with Bluetooth 5.0")

    def receive_price(self):
        ''''

        describe method to receive a price of the Tefal Smartscales

        '''
        print("The price of a Tefal Smartscales is 1800 gr")
