from XiomiSS import Xiomi
from TefalSS import Tefal

if __name__ == "__main__":
    #create object of a class Xiomi and asign values to all parameters
    xiomi = Xiomi(180, "Bluetooth 4.0", "kg, lb", 16, 2000)
    #create object of a class Tefal and asign values to all parameters
    tefal = Tefal(180, "Bluetooth 5.0", "kg, lb", 16, 1800)
    xiomi.receive_price()
    xiomi.receive_data()
    xiomi.transfer_data()
    tefal.receive_price()
    tefal.receive_data()
    tefal.transfer_data()




