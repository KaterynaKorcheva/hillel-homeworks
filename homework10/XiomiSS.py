from SmartScalesBase import SmartScales

class Xiomi(SmartScales):
    ''''
    daughter's class to describe SmartScale Xiomi
    '''
    def __init__(self, max_weight, bluetooth, measure_units, number_users, price):
        super().__init__(max_weight, bluetooth, measure_units, number_users, price)


    def receive_data(self):
        ''''
        describe method to receive data by phone
        '''
        print("Recieve data to your phone")

    def transfer_data(self):
        ''''
        decribe method to transfer data by bluetooth version
        '''
        print("Connection with Bluetooth 4.0")

    def receive_price(self):
        ''''

        describe method to receive a price of the Xiomi Smartscales

        '''
        print("The price of a Xiomi Smartscales is 2000 gr")
