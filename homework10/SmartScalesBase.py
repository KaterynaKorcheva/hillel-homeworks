from abc import ABC, abstractmethod


class SmartScales(ABC):
    ''''
      parents class describes behaviur of object  SmartScales, basic info
    '''
    def __init__(self, max_weight: int, bluetooth: str, measure_units: str, number_users: int, price: int):
        ''''
        \brief simple class initializer

        \param max_weight         Describes the max possibility weight
        \param bluetooth          Describes the path of sending data
        \param measure_units      Describes units of a measure
        \param number_users       Describes quality of users

        '''
        self.max_weight = max_weight
        self.bluetooth = bluetooth
        self.measure_units = measure_units
        self.number_users = number_users
        self.price = price

    def measure_weight(self):
        ''''
        function definition for measure the weight
        '''
        print("Your weight is...")


    def connect_cloud(self):
        ''''
        function definition for connect to the cloud for sending results
        '''
        print("Your data is ready to transfer  to the cloud")

    @abstractmethod
    def transfer_data(self):
        ''''
        function definition for transfer results
        '''
        pass

    @abstractmethod
    def receive_data(self):
        ''''
        function definition for receive results by device
        '''
        pass

    @abstractmethod
    def receive_price(self):
        ''''
        function definition to receive a price of the device
        '''
        pass
