import time

def time_of_function(process_age):
    def wrapped(*args):
        start_time = time.perf_counter_ns()
        res = process_age(*args)
        print(f"Execution time is: {(time.perf_counter_ns() - start_time) / 1000 / 1000 / 1000} sec")
        return res
    return wrapped
