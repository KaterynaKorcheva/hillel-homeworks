from abc import ABC, abstractmethod


class Item(ABC):
    @abstractmethod
    def describe(self):
        pass


class ItemBase(Item):
    def __init__(self, _type):
        self._type = _type

    def describe(self):
        return f"This is {self._type}"
