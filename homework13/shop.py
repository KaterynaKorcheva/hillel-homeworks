from singeltone_shop import singletone
from buscuit import ItemBiscuit
from apple import ItemApple
from buckwheat import ItemBuckwheat

class Shop:
    ''''
    describe Shop class, basic info
    '''
    def __init__(self):
        pass

    @staticmethod
    def get_item(item_type:str):
        ''''
        \brief getter for items
        '''
        if item_type == "buckwheat":
            return ItemBuckwheat()
        elif item_type == "apple":
            return ItemApple()
        elif item_type == "biscuit":
            return ItemBiscuit()
        else:
            raise Exception("Incorrect item name")





