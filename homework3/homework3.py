#1.Напишіть цикл, який буде вимагати від користувача ввести слово, в якому є буква "о" (враховуються
#як великі так і маленькі). Цикл не повинен завершитися, якщо користувач ввів слово без букви "о".

while 1 != 0:
    print("Enter a word")
    word = input()
    wordlow= word.lower()
    if wordlow.find('o') != -1 :
        print ("Precess will be finished!!!")
        break




#2. Є list з даними lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть код, який свормує новий list (наприклад lst2), який містить лише змінні типу стрінг, які присутні в lst1.
# Зауважте, що lst1 не є статичним і може формуватися динамічно від запуску до запуску.

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = []
for elem in lst1:
    if type(elem) == str:
        lst2.append(elem)
print(lst2)

#3.Є стрінг з певним текстом (можна скористатися input або константою).
#Напишіть код, який визначить кількість слів в цьому тексті, які закінчуються на "о"
# (враховуються як великі так і маленькі).

text = input("Please,enter text")
word_list = text.split()
number = 0

for elem in word_list:
    word = elem.lower()
    if word[-1] == 'o':
        number = number + 1
print(number)




